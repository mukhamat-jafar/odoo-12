import json
from xmlrpc import client
from odoo import http
from odoo.tools import config
from odoo.http import Response

class KendaraanController(http.Controller):
    odoo_url= config.get("odoo_host")
    db = config.get("db_name")
    username=config.get("odoo_user")
    password = config.get("odoo_password")

    def init_model(self):
        common = client.ServerProxy('{}/xmlrpc/2/common'.format(self.odoo_url))
        uid = common.authenticate(self.db, self.username, self.password, {})
        models = client.ServerProxy('{}/xmlrpc/2/object'.format(self.odoo_url))
        return [models, uid]

    @http.route('/kendaraan', auth='public', type="http", methods=['GET'])
    def get_all(self, **kwargs):
        [models, uid] = self.init_model()
        resource = models.execute_kw(self.db, uid, self.password, 'kendaraan', 'search_read', [kwargs])
        headers = {'content-type': 'application/json'}
        return Response(json.dumps(resource), headers=headers)

    @http.route('/kendaraan/<int:resource_id>', auth='public', type='http', methods=['GET'])
    def get_by_id(self, resource_id):
        [models, uid] = self.init_model()
        resource = models.execute_kw(self.db, uid, self.password, 'kendaraan', 'search_read', [[['id', '=', resource_id]]])
        headers = {'content-type': 'application/json'}
        return Response(json.dumps(resource), headers=headers)

    @http.route('/kendaraan', auth='public', type='json', methods=['POST'])
    def create(self, **kwargs):
        [models, uid] = self.init_model()
        resource = models.execute_kw(self.db, uid, self.password, 'kendaraan', 'create', [kwargs])
        return resource

    @http.route('/kendaraan/<int:resource_id>', auth='public', type='json', methods=['PATCH'])
    def update(self, resource_id, **kwargs):
        [models, uid] = self.init_model()
        resource = models.execute_kw(self.db, uid, self.password, 'kendaraan', 'write', [[resource_id], kwargs])
        return resource

    @http.route('/kendaraan/<int:resource_id>', auth='public', type='http', methods=['DELETE'], csrf=False)
    def delete(self, resource_id):
        [models, uid] = self.init_model()
        resource = models.execute_kw(self.db, uid, self.password, 'kendaraan', 'unlink', [[resource_id]])
        headers = {'content-type': 'application/json'}
        return Response(json.dumps({'result': resource}), headers=headers)