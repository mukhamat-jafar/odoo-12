{
    'name': 'Student Module',
    'version': '1.1.1',
    'summary': 'Model Student',
    'description': 'Model Student',
    'category': 'document',
    'author': 'equity',
    'website': 'www.equity.co.id',
    'license': 'AGPL-3',
    'depends': [
        'base'
    ],
    'data': [
        "security/ir.model.access.csv",
        "view/student_view.xml"
    ],
    'installable': True,
    'auto_install': False
}