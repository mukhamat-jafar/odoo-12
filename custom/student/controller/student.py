import json
from xmlrpc import client

from odoo import http
from odoo.tools import config
from odoo.http import Response

class StudentController(http.Controller):
    odoo_url = config.get("odoo_host")      #URL Database Odoo
    db = config.get("db_name")              #DB Yg Digunanakan
    username = config.get("odoo_user")      #Username DB
    password = config.get("odoo_password")  #Password DB

    def init_model(self):
        common = client.ServerProxy('{}/xmlrpc/2/common'.format(self.odoo_url))
        uid = common.authenticate(self.db, self.username, self.password, {})
        models = client.ServerProxy('{}/xmlrpc/2/object'.format(self.odoo_url))
        return [models, uid]

    @http.route('/students', auth='public', type='http', methods=['GET'], csrf=False)
    def get_all(self, **kwargs):
        [models, uid] = self.init_model()
        resource = models.execute_kw(self.db, uid, self.password, 'student', 'search_read', [kwargs])
        headers = {'content-type': 'application/json'}
        return Response(json.dumps(resource), headers=headers)

    @http.route('/students/<int:resource_id>', auth='public', type='http', methods=['GET'], csrf=False)
    def get(self, resource_id):
        [models, uid] = self.init_model()
        resource = models.execute_kw(self.db, uid, self.password, 'student', 'search_read',
                                     [[['id', '=', resource_id]]])
        headers = {'content-type': 'application/json'}
        return Response(json.dumps(resource), headers=headers)

    @http.route('/students', auth='public', type='json', methods=['POST'], csrf=False)
    def create(self, **kwargs):
        [models, uid] = self.init_model()
        resource = models.execute_kw(self.db, uid, self.password, 'student', 'create', [kwargs])
        return resource

    @http.route('/students/<int:resource_id>', auth='public', type='json', methods=['PATCH'], csrf=False)
    def update(self, resource_id, **kwargs):
        [models, uid] = self.init_model()
        resource = models.execute_kw(self.db, uid, self.password, 'student', 'write',[[resource_id], kwargs])
        return resource

    @http.route('/students/<int:resource_id>', auth='public', type='http', methods=['DELETE'], csrf=False)
    def remove(self, resource_id):
        [models, uid] = self.init_model()
        resource = models.execute_kw(self.db, uid, self.password, 'student', 'unlink', [[resource_id]])
        headers = {'content-type': 'application/json'}
        return Response(json.dumps({'result': resource}), headers=headers)