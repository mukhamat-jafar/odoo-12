from odoo import fields, models, api

class Student(models.Model):
    _name = "student"
    _description = "Student Model"

    id = fields.Integer(
        string="ID",
        required=False
    )

    name = fields.Char(
        string="Name",
        required=False,
        size=200
    )
