from odoo import models, fields

class KaryawanModel(models.Model):
    _name = "karyawan_db"

    id = fields.Integer(
        string="ID",
        required=False
    )

    nama_karyawan = fields.Char(
        string="Nama Karyawan",
        required=False
    )

    alamat = fields.Char(
        string="Alamat",
        required=False
    )

